﻿using Dribbble.Model.Entity;
using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Windows.Phone.Speech.Synthesis;
using Microsoft.Phone.Shell;
namespace Dribbble.Supports
{
    public class Util
    {
        private IsolatedStorageSettings db = IsolatedStorageSettings.ApplicationSettings;


        public RootObject ObterPagina(string pagina)
        {
            string json = string.Empty;

            if (db.Contains("rootObjectCollection"))
            {
                db.TryGetValue("rootObjectCollection", out json);
            }

            List<RootObject> rootObjectCollection = new List<RootObject>();

            if (!string.IsNullOrWhiteSpace(json))
            {
                rootObjectCollection = JsonConvert.DeserializeObject<List<RootObject>>(json);
            }

            return rootObjectCollection.FirstOrDefault(x => x.page == pagina);
        }

        public void GravarPagina(RootObject root)
        {
            string json = string.Empty;
            if (db.Contains("rootObjectCollection"))
            {
                db.TryGetValue("rootObjectCollection", out json);
            }

            List<RootObject> rootObjectCollection = new List<RootObject>();

            if (!string.IsNullOrWhiteSpace(json))
            {
                rootObjectCollection = JsonConvert.DeserializeObject<List<RootObject>>(json);
            }


            if (!rootObjectCollection.Any(x => x.page == root.page))
            {

                rootObjectCollection.Add(root);

                json = JsonConvert.SerializeObject(rootObjectCollection);

                db.Remove("rootObjectCollection");
                db.Add("rootObjectCollection", json);
                db.Save();
            }
        }

        public Shot ObterShotSelecionado()
        {
            string json = string.Empty;

            if (db.Contains("shotSelecionado"))
            {
                db.TryGetValue("shotSelecionado", out json);
            }

            Shot shot = new Shot();

            if (!string.IsNullOrWhiteSpace(json))
            {
                shot = JsonConvert.DeserializeObject<Shot>(json);
            }

            return shot;
        }

        public void GravarShotSelecionado(Shot shot)
        {
            string json = JsonConvert.SerializeObject(shot);

            db.Remove("shotSelecionado");
            db.Add("shotSelecionado", json);
            db.Save();

        }

        public string RemoveTagHTML(string texto)
        {
            texto = Regex.Replace(texto, @"<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;", string.Empty).Trim();

            return texto;
        }

        public void AtualizarTile(RootObject rootObject)
        {
            int qtdeNoticiaNaoLidas = 0;
            
                qtdeNoticiaNaoLidas = rootObject.total;

                var meuTile = ShellTile.ActiveTiles.First();
                var standardTile = new StandardTileData
                {
                    Title = "",
                    //BackgroundImage = new Uri("arroba.png", UriKind.Relative),
                    Count = qtdeNoticiaNaoLidas,
                    BackTitle = "Ultima página visualizada",
                    BackContent = rootObject.page
                    //BackBackgroundImage = new Uri("arroba_back.png", UriKind.Relative)
                };
                meuTile.Update(standardTile);
            
        }



        public async void FalarTexto(string textoQueDesejarFalar = "Falta definir um texto a ser falado!")
        {
            try
            {
                SpeechSynthesizer synth = new SpeechSynthesizer();

                // Query for a voice that speaks French.
                IEnumerable<VoiceInformation> voices = from voice in InstalledVoices.All
                                                       where voice.Language == "pt-BR" && voice.Gender == VoiceGender.Male
                                                       select voice;

                // Set the voice as identified by the query.
                synth.SetVoice(voices.ElementAt(0));

                // Count in French.
                await synth.SpeakTextAsync(textoQueDesejarFalar);
            }
            catch (Exception ex)
            {

            }


        }
    }
}
