﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Dribbble.Resources;
using Dribbble.Model.Entity;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using Dribbble.Supports;
using Microsoft.Phone.Tasks;

namespace Dribbble
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        private int? pagina;
        private Util util = new Util();
        private bool primeiroAcesso = true;
        public MainPage()
        {
            InitializeComponent();

            this.Loaded += MainPage_Loaded;
        }


        void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (primeiroAcesso==true)
            {
                util.FalarTexto("Olá, sejam bem vindos.");
                primeiroAcesso = false;
            }
            
            CarregarListaDeShots();
        }




        private void btnAnterior_Click(object sender, EventArgs e)
        {
            if (pagina.HasValue)
            {
                pagina = pagina - 1;
            }

            if (pagina==0)
            {
                pagina = 1;
                util.FalarTexto("Você já se encontra na primeira página.");
                return;
            }
            util.FalarTexto("Aguarde, carregando a página anterior.");
            CarregarListaDeShots();
            
        }

        private void btnProximo_Click(object sender, EventArgs e)
        {
            if (pagina.HasValue)
            {
                pagina = pagina + 1;
            }
            util.FalarTexto("Aguarde, carregando a próxima página.");
            CarregarListaDeShots();
        }
        #region Paginação
        private void CarregarListaDeShots()
        {
            if (!pagina.HasValue)
            {
                pagina = 1;
            }

            UxtxbPagina.Text = pagina.ToString();

            //Chamar API Via Cache
            RootObject rootObject = ChamarApiViaCache();

            if (rootObject != null)
            {
                CarregarRootObjectNaLista(rootObject);
                util.AtualizarTile(rootObject);
            }
            else
            {
                ChamarAPIViaRest();
            }
        }

        private void ChamarAPIViaRest()
        {
            Uri uri = new Uri(string.Concat("http://api.dribbble.com/shots/popular?page=", pagina));
            WebClient webClient = new WebClient();
            webClient.DownloadStringCompleted += (s, e) =>
            {
                if (e.Error != null)
                {
                    MessageBox.Show("Operação falhou!");
                }
                else
                {
                    string resultJSON = e.Result;

                    //Mapeamento JSON para Object
                    RootObject rootObject = JsonConvert.DeserializeObject<RootObject>(resultJSON);

                    util.GravarPagina(rootObject);

                    CarregarRootObjectNaLista(rootObject);
                    util.AtualizarTile(rootObject);
                }

            };
            webClient.DownloadStringAsync(uri);
        }

        private RootObject ChamarApiViaCache()
        {
            RootObject rootObject = util.ObterPagina(pagina.ToString());

            return rootObject;
        }

        private void CarregarRootObjectNaLista(RootObject root)
        {
            if (root != null)
            {
                root.shots.ForEach(x =>
                {
                    //Remove tags html
                    if (!string.IsNullOrWhiteSpace(x.description))
                    {
                        x.description = new Util().RemoveTagHTML(x.description);
                    }
                });
            }

            UxDblShot.ItemsSource = root.shots;
        }
        #endregion


        private void spRoot_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Shot shotCollection = ((StackPanel)sender).DataContext as Shot;
            
            if (shotCollection != null)
            {
                util.GravarShotSelecionado(shotCollection);
                
                NavigationService.Navigate(new Uri("/Detalhe.xaml", UriKind.Relative));
            }
        }

        private void spCompatilhar_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Shot shotCollection = ((StackPanel)sender).DataContext as Shot;

            if (shotCollection != null)
            {
                util.FalarTexto("Tem certeza que deseja compartilhar está matéria com seus amigos?");
                MessageBoxResult resultado = MessageBox.Show("Tem certeza que deseja compartilhar este artigo com seus amigos?", "", MessageBoxButton.OKCancel);
                if (resultado == MessageBoxResult.OK)
                {
                    ShareLinkTask shareLinkTask = new ShareLinkTask();
                    shareLinkTask.LinkUri = new Uri(shotCollection.url, UriKind.Absolute);

                    shareLinkTask.Title = "Dribbble" + shotCollection.description;
                    shareLinkTask.Show();
                }
            }
        }
    }
}