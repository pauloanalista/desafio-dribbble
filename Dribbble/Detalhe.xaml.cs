﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Dribbble.Model.Entity;
using Dribbble.Supports;

namespace Dribbble
{
    public partial class Detalhe : PhoneApplicationPage
    {
        Util util = new Util();
        public Detalhe()
        {
            InitializeComponent();

            this.Loaded += Detalhe_Loaded;
        }

        void Detalhe_Loaded(object sender, RoutedEventArgs e)
        {
            Shot shot = util.ObterShotSelecionado();

            if (shot == null)
            {
                return;
            }
            LayoutRoot.DataContext = shot;

            //UxImbTitulo.ImageSource = shot.image_400_url;
            //UxTxbTitulo.Text = shot.title;
            //UxTxbNome.Text = shot.player.name;


        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            if (NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }
    }
}